---
title: "Black Bean Tostadas"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
tags: 
- beans
- chicken breast
- corn tortillas
- guacamole
- yogurt
---

## Ingredients 

- 8 oz. grilled chicken breast (or cook 10 oz. raw)
- 8 thin corn tortillas
- 2/3 cup black or refried beans
- 4 tsp. Taco seasoning
- 1 red bell pepper
- 2 green onions, chopped
- 8 Tbs. low-fat, shredded mozzarella cheese
- Shredded lettuce
- 8 oz. guacamole
- 4 Tbs. non-fat, plain Greek yogurt
- 4 Tbs. Salsa
- 1 lime, juice of

## Directions 

1. Preheat oven to 375 degrees. Line baking sheet with foil. Spray foil with cooking spray. Lay tortillas in a single layer on foil and spray tops with cooking spray. Sprinkle tops of tortillas with taco seasoning. Bake for 8 minutes.
2. Drain and rinse beans. Place on a plate and microwave 30-45 seconds. Mash with a fork, then spread on warm tortillas. Slice bell peppers, grilled chicken and green onions. Layer evenly on tortillas and top each one with 1 Tbs. shredded cheese.
3. Return to oven for another 8 minutes. 2 tostadas per serving. Enjoy each serving warm with 2 oz. guacamole, 1 Tbs. Greek yogurt, 1 Tbs. salsa and a squirt of lime juice.