---
title: "Wild Rice and Kale Salad"
date: 2020-12-16T19:38:07-08:00
categories:
- Entrees
- Salad
tags:
- chicken breast
- kale
- avocado
- cilantro
- apples
---

## Ingredients 

Salad (about 8 servings):
- wild rice (cooked on stove 1 cup rinsed rice to 1.5 cups water--use about ¾ of the cooked rice)
- ¾-1 bunch of washed, shredded kale 
- ⅓ cup? slivered almonds
- 2 avocados 
- 1-2 apples 
- ½ cup- 1 cup dried cranberries
- 2-3 grilled chicken breast halves, seasoned with salt and pepper
- feta cheese

Balsamic Lemon Cilantro Vinaigrette:
- ¼ tsp. garlic powder
- 3 tablespoons minced onion
- ¼ cup honey
- ⅓ cup olive oil
- 1.5 Tbs. lemon
- 3 tablespoons balsamic vinegar
- ¼ teaspoon ground pepper
- 1 teaspoon salt
- ¼ cup (packed) cilantro

## Directions 

Blend dressing ingredients until smooth. Toss the salad in a serving bowl with the dressing. Keeps ok as leftovers the next day. 