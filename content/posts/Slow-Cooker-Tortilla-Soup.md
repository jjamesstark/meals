---
title: "Slow Cooker Tortilla Soup"
date: 2020-12-16T19:35:31-08:00
categories:
- Entrees
- Soup
tags:
- chicken breast
- enchilada sauce
- chicken broth
- green chile peppers
- black beans
---

## Ingredients 

- 1 pound chicken breast
- 1 (15 ounce) can petite diced tomatoes
- 1 (10 ounce) can enchilada sauce
- 1 (14.5 ounce) can chicken broth
- 1 (4 ounce) can chopped green chile peppers
- 1 teaspoon cumin
- 1 teaspoon chili powder
- 1 teaspoon salt
- 1/4 teaspoon black pepper
- 1 bay leaf

- 1 medium onion, chopped
- 3 cloves garlic, minced
- 1 cup frozen corn
- 1 tablespoon chopped cilantro
- 1 can black beans

## Directions 

1. Place chicken, tomatoes, enchilada sauce, broth, green chiles, and dried spices into a slow cooker. Cover, and cook on Low setting for 6 to 8 hours or on High setting for 3 to 4 hours.
1. Pull out the chicken breasts and shred with 2 forks. Put back in the pot. 
1. Saute onion and garlic in a bit of oil and add to the pot. 
1. Add corn and let the whole mixture stew for another 10+ minutes. 

Serve with cilantro, cheese/sour cream  and chips.