---
title: "Pumpkin Spice Waffles"
date: 2020-12-16T20:19:33-08:00
categories:
- Breakfast
- Waffles
tags:
- waffles
---

## Ingredients 

- 1 1/2 cups all-purpose flour
- 3 teaspoons baking powder
- 1/2 teaspoon baking soda
- 1 teaspoon cinnamon
- 1 teaspoon nutmeg
- 1 teaspoon ground ginger
- 1 pinch salt
- 2 eggs
- 1/4 cup firmly packed brown sugar
- 1 cup canned pumpkin puree
- 1 cup milk (add more if needed)
- 4 tablespoons vegetable oil

## Directions 

1. Mix together flour, baking powder, baking soda, cinnamon, nutmeg, ginger, and salt in large bowl.
1. In a second bowl, add eggs, sugar, pumpkin, milk, and butter; beat well.
1. Gently fold in the flour mixture.
1. Cook according to your waffle iron directions.

I do these in my belgian waffle iron and it uses about 1 cup batter and takes 4-5 minutes to bake. They come out a nice deep, golden brown. These are great with a little bit of syrup, but would also be great with honey butter.
