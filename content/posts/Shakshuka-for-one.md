---
title: "Shakshuka for One"
date: 2020-12-16T20:01:45-08:00
categories:
- Breakfast
tags:
- jalapeno
- feta cheese
- eggs
---

## Ingredients 

- 2 Tbs olive oil
- 1 jalapeno, stemmed, seeded, and finely chopped 
- ½ small yellow onion, chopped
- 3 cloves garlic, crushed then sliced
- ½ teaspoon ground cumin
- ½ tablespoon paprika
- 1 teaspoon dried parsley
- 1 14 oz. can petite diced tomatoes, undrained
- ¼ cup water
- Kosher salt, to taste (try ¼ tsp?)
- 2 eggs (per serving)
- feta cheese, crumbled

## Directions 

Heat oil in a 12-inch skillet over medium-high heat. Add jalapeno and onions and cook, stirring occasionally, until soft and golden brown, about 6 minutes. Add garlic, cumin, paprika, and parsley, and cook, stirring frequently, until garlic is soft, about 2 more minutes.

Add crushed tomatoes and their liquid to skillet along with 1/4 cup water, reduce heat to medium, and simmer, stirring occasionally, until thickened slightly, about 10 minutes. Season with salt.

Remove ⅔ of the mixture to put in the fridge for other days. 

Rake the remaining mixture together in the frying pan. Crack eggs over sauce, into the wells so that eggs are evenly distributed across sauce’s surface. Cover skillet and cook over medium-low until yolks are just set, about 5 minutes. Using a spoon, baste the whites of the eggs with tomato mixture, being careful not to disturb the yolk. Sprinkle shakshuka with feta.