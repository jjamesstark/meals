---
title: "Pepperoni Pizza Pinwheels"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
tags: 
- broccoli
---

## Ingredients 

- ¾ cup self-rising flour
- ½ cup plain, non-fat Greek yogurt
- ½ cup traditional marinara sauce
- 5 oz. turkey pepperoni
- 1 cup mozzarella cheese, shredded
- 4 cups broccoli
- ¼ cup nutritional yeast
- Sea salt

## Directions 

1. Preheat oven to 450 degrees.
2. Place flour, 3 Tbs. nutritional yeast and Greek yogurt in a mixer and combine/knead using the hook attachment. If you don’t have a mixer, stir the ingredients together in a large bowl and knead with hands for 3-5 minutes. If dough is too dry, add 1 Tbs. of water.
3. Roll dough out into a rectangular shape. Spread marinara sauce evenly on dough, add pepperoni and cheese. Carefully roll dough up lengthwise and slice into four equal pinwheels. Place on a greased baking sheet. Bake for 12-15 minutes. Enjoy 1 cup of roasted broccoli on the side.
4. Chop broccoli into florets and place on a baking sheet lined with parchment paper. Spray tops with cooking spray, then sprinkle with sea salt and one Tbs. nutritional yeast (this will give it a cheesy flavor + added protein). Bake for 15-20 minutes