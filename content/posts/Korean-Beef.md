---
title: "Korean Beef"
date: 2020-12-16T19:25:14-08:00
categories:
- Entrees
tags:
- flank steak
- red cabbage
---

## Ingredients 

- 4 Tbsp (1/4 cup) sesame oil (next time try 3 Tbs)
- 4 garlic cloves, minced
- 1 Tbsp fresh ginger, grated (optional but nice)
- 1/4 cup soy sauce
- 1/4 cup packed light brown sugar (press it into the cup)
- 1 tsp ground black pepper
- 1/4 cup hot water
- 1 1/2 lb flank steak, hanger steak, top sirloin, or flat iron steak
- red cabbage

## Directions 

1. Thinly slice beef against the grain and place in medium bowl.
1. Stir together all marinade ingredients until sugar dissolves, pour over sliced meat and stir to combine. Cover and refrigerate 4 hours or overnight for best results. Tip: Let beef stay at room temp 30 min before cooking. Chilled meat will cool a hot pan and make your meat juice out.
1. Heat a large, heavy non-stick pan on high heat (no need to add extra oil). It's hot enough when a piece of meat placed in the center has a strong sizzle. Add meat and sauté without stirring for 2-3 min or until lightly browned then stir and continue cooking additional 3-5 min, stirring occasionally or until fully cooked, liquid has evaporated and the sauce has caramelized on the beef.

Serve over rice with lettuce, cabbage, and sriracha mayo sauce (mayo, sriracha, sugar), green onions, sesame seeds
