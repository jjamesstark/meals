---
title: "Harvest Cobb Salad"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
- salad
tags: 
- salad
- chicken breast
- butternut squash
---

## Ingredients 

- 8 cups spinach
- 8 cups romaine lettuce
- 8 hard boiled eggs, sliced
- 8 oz chicken breast
- 8 slices turkey bacon, cooked and chopped
- 1/2 cup pecans, chopped
- 8 cups butternut squash, cubed and roasted
- salt, to taste
- pepper, to taste

**For Dressing:**
- 1/2 cup lowfat mayo
- 2 Tbs. lemon juice
- 2 Tbs. raw honey
- 2 Tbs. Dijon mustard
- 1 tsp. dry mustard

## Directions 

1. Roast the squash at 400 for 25 minutes, sprayed or tossed with oil.
1. Boil eggs (boil water, add eggs, cover pot, turn water off for 11 minutes. Then cool eggs in an ice bath)
1. Chop and saute bacon in a frying pan. 
1. Salt, pepper, and grill chicken breast halves.
1. Blend all the dressing ingredients in a blender until smooth. 
1. Assemble salads, dividing each component into 8 servings. Drizzle dressing.