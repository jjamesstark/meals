---
title: "Hot Ham Cheese Sandwiches"
date: 2020-12-16T20:36:53-08:00
categories:
- Appetizers
tags:
- sweet rolls
- ham
- provolone
- poppyseed
---

## Ingredients 

- 1 pkg Hawaiian sweet rolls
- 1 lb honey maple ham, shaved
- 1 (8 oz) pre-sliced swiss or provolone cheese (we prefer provolone)
- ½ cup butter
- ½ tsp worchestershire sauce
- 1 ½ tsp mustard
- 1 tsp garlic powder
- 1 ½ tbsp poppyseed or chopped chives (optional)

## Directions 

1. Split rolls in half and sit the bottoms in a 9x13 pan.
1. Place the ham on the bottoms followed by the cheese and roll tops.
1. Melt butter and combine with worchestershire, mustard and garlic until combined. 
1. Stir in poppyseed.
1. Drizzle mixture over rolls in pan.
1. Bake at 350 for 15 minutes or until cheese is melted.