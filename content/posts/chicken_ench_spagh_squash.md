---
title: "Chicken Enchilada Stuffed Spaghetti Squash"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
tags: 
- spaghetti squash
- chicken breast
---

## Ingredients 

- 4 cups cooked spaghetti squash
- 8 oz. cooked and shredded chicken breast
- 8 oz. green chilies, canned, chopped
- 4 lite Laughing Cow cheese wedges
- ½ cup black beans, drained
- 1 cup enchilada sauce, red or green
- ½ cup lowfat, shredded mozzarella cheese
- 120g avocado
- Green onions, for garnish
- Side per serving:
- 75g apples or pears, sliced"

## Directions 

1. Heat oven to 425 degrees. Cut each spaghetti squash in half lengthwise and spoon out the seeds. Place face down on a baking sheet lined with parchment paper. Bake for 45 minutes or until tender.
2. Combine shredded chicken, green chilies, cheese wedges and black beans. Rake cooked spaghetti squash into a large bowl with a fork. Place one cup spaghetti squash back into each shell then add ¼ of chicken mixture in each one. Top each serving with ¼ cup enchilada sauce and 2 Tbs. shredded cheese.
3. Turn oven down to 350. Return stuffed spaghetti squash back to oven and bake another 10 minutes until cheese is melted and chicken mixture is heated through. Top each with 30g chopped avocado, green onions, salt and pepper to taste. Enjoy fruit on the side.