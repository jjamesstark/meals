---
title: "Brotein Pancakes"
date: 2020-12-16T20:04:30-08:00
categories:
- Breakfast
- Pancakes
tags:
- whey protein
- yogurt
- banana
---

## Ingredients 

- 2 eggs (or 4 whites)
- 1 ripe banana
- ½ cup oats
- 1 scant scoop protein powder (whey, chocolate flavored)
- ¼ cup plain yogurt
- 1 tsp. Baking powder
- 1 tsp. Vanilla
- ¼ tsp salt

## Directions 

Combine all ingredients in a blender. Cook on a hot pan/griddle. 
