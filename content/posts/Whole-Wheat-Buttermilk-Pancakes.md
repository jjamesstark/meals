---
title: "Whole Wheat Buttermilk Pancakes"
date: 2020-12-16T19:57:36-08:00
categories:
- Breakfast
- Waffles
tags:
- waffles
---

## Ingredients 

wet:
- 3 cups buttermilk 
- 3 large eggs
- 6 Tbs melted butter

dry:
- 12-13 oz whole wheat flour
- ½ tsp salt
- ¾ tsp baking soda
- 1 ½ tsp baking powder
- 2 Tbs. white sugar

## Directions 

Whisk dry and wet ingredients separately. Combine mixing until just incorporated. Lumps are ok. 
