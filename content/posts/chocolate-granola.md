---
title: "Chocolate Granola"
date: 2020-12-16T20:33:30-08:00
categories:
- Breakfast
tags:
- granola
- oats
- coconut
- walnut

---

 - Yields 7 cups

## Ingredients 

- 4 Cups rolled oats
- 1 cup shredded coconut 
- 1 cup chopped, raw walnut halves or pieces (or any nuts you have!)
- 1 cup puffed rice
- ½ teaspoon salt 
- ½ cup, plus 1 tablespoon cocoa powder 
- 1/3 cup melted butter
- ½ teaspoon vanilla extract
- 1/3 cup mild tasting honey 
- 1/4 cup light brown sugar

## Directions 

1. Preheat oven to 325 degrees Fahrenheit. 
1. In a large bowl, stir together the oats, coconut, walnuts, seeds, and salt. In a medium bowl, whisk together the cacao, coconut oil, vanilla, honey, and coconut sugar until smooth. Pour cocoa mixture over the oat mixture and stir until oats are coated. I find using two rubber spatulas at the same time to mix the ingredients works best. If you like your granola very clumpy, add more honey. 
1. Coat one large or two small rimmed baking sheet(s) with cooking spray. Press granola into the bottom of the prepared baking sheet and bake for 15 minutes. Stir and bake another 10-15 minutes. Cool completely in the pan without stirring. Granola will crisp up, clump, and harden as it cools. Store in an airtight container. 
