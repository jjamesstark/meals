---
title: "Crispy Whole Wheat Waffles"
date: 2020-12-16T20:07:21-08:00
categories:
- Breakfast
- Waffles
tags:
- waffles
---

## Ingredients 

- 1 1/2 cups Whole Wheat Flour
- 2 teaspoons baking powder
- 1/2 teaspoon salt
- 2 tablespoons sugar
- 1 large egg
- 1 1/2 cups lukewarm milk
- 1/3 cup melted butter or vegetable oil

## Directions 


1. Preheat your iron while you make the waffle batter.
1. Whisk together the flour, baking powder, salt, and sugar.
1. In a separate bowl, whisk together the egg, milk, and butter or oil.
1. Mix together the wet and dry ingredients, stirring just until combined. The batter will be a bit lumpy; that's OK.
1. Cook the waffles as directed in the instructions that came with your waffle iron.

- Yield: 3 1/2 round Belgian-style (deep-pocket) 7" waffles.

## sources

(banana oatmeal pancakes-whole wheat)
http://allrecipes.com/recipe/whole-wheat-oatmeal-and-banana-pancakes/detail.aspx?event8=1&prop24=SR_Title&e11=whole%20wheat%20pancakes&e8=Quick%20Search&event10=1

http://www.inspiredtaste.net/17109/spiced-buttermilk-banana-pancakes-recipe/
